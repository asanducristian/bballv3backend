var express = require('express');
var router = express.Router();
const multer = require("multer");
const path = require("path");
const fs = require("fs");

const upload = multer({
    dest: "./profilePictures/"
});

router.use(function timeLog(req, res, next) {
    console.log('Time: ', Date.now())
    next()
})

router.post(
    "/",
    upload.single("photo"),
    (req, res) => {
        const tempPath = req.file.path;
        const targetPath = path.join(__dirname, "./uploads/image.png");

        if (path.extname(req.file.originalname).toLowerCase() === ".png") {
            fs.rename(tempPath, targetPath, err => {
                if (err) {
                    sendJsonResponse(res, req, false, "Something went wrong (1)", null, 500);
                } else {
                    sendJsonResponse(res, req, true, "Successfully uploaded photo!", null, 200);
                }
            });
        } else {
            fs.unlink(tempPath, err => {
                if (err) {
                    sendJsonResponse(res, req, false, "Something went wrong (2)", null, 500);
                } else {
                    sendJsonResponse(res, req, false, "Only .png files are allowed!", null, 403);
                }
            });
        }
    }
);


const generateAuthToken = () => {
    return crypto.randomBytes(30).toString('hex');
}



function sendJsonResponse(res, req, success, message, data, code) {
    var jsonResponse = {};
    jsonResponse["success"] = success;
    jsonResponse["message"] = message;
    jsonResponse["data"] = data;
    res.setHeader("auth-token", req.header("auth-token") == undefined ? "" : req.header("auth-token"));
    res.status(code).send(jsonResponse);
}
module.exports = router