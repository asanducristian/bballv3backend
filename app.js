const express = require('express');
const bodyParser = require('body-parser');
const sqlite3 = require('sqlite3').verbose();
const crypto = require('crypto');
const multer = require("multer");
const path = require("path");
const fs = require("fs");
const OneSignal = require('onesignal-node');

const client = new OneSignal.Client('bdacfb8a-1909-448f-9ee8-af57e03915c8', 'MjgzYTlhOTctOTJiMC00ODdlLWE1ZTctNzBmYjIyMGFkMzc0');

const upload = multer({
    dest: "./profilePictures/"
});

const app = express();
app.use(express.static('uploads'))

let db = new sqlite3.Database('./db/bball2', sqlite3.OPEN_READWRITE, (err) => {
    if (err) {
        console.error(err.message);
    } else {
        db.get("PRAGMA foreign_keys = ON");
        console.log('Connected to the test database.');
    }
});

let authTokens = {};

app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());

app.listen(8081, () => {
    console.log('Server started on port 8081...');
})

//APIS
app.get('/', async (req, res) => {
    await sendJsonResponse(res, req, true, "Server was successfully reached!", null, 200)
});

app.post('/api/register', async (req, res) => {
    var full_name = req.body.email;
    if (req.body.full_name != null) {
        full_name = req.body.full_name;
    }
    db.run(`INSERT INTO users(full_name, email, password) VALUES("${full_name}", "${req.body.email}", "${getHashedPassword(req.body.password)}")`, function (err) {
        if (err) {
            sendJsonResponse(res, req, false, err.message, null, 400);
        } else {
            db.all(`SELECT * FROM users where id = ${this.lastID} LIMIT 1`, (err, row) => {
                if (err || row == "") {
                    sendJsonResponse(res, req, false, err.message, null, 400);
                } else {
                    var result = {};
                    result["user"] = row[0];
                    var jsonResponse = {};
                    jsonResponse["success"] = true;
                    jsonResponse["message"] = "Successfully created user!";
                    jsonResponse["data"] = result;
                    var token = generateAuthToken();
                    authTokens[token] = result["user"];
                    res.setHeader("auth-token", token);
                    res.status(200).send(jsonResponse);
                }
            });
        }
    });
});

app.post('/api/edit-user', async (req, res) => {
    checkLogin(req.header("auth-token"), function (user) {
        db.run(`UPDATE users SET ${req.body.full_name != null ? `full_name = "${req.body.full_name}",` : ``}${req.body.gender != null ? `gender = "${req.body.gender}",` : ``}${req.body.email != null ? `email = "${req.body.email}",` : ``}${req.body.age != null ? `age = "${req.body.age}",` : ``}${req.body.sport_id != null ? `sport_id = ${req.body.sport_id},` : ``}${req.body.city_id != null ? `city_id = ${req.body.city_id}` : ``} where id=${user.id}`, function (err) {
            if (err) {
                sendJsonResponse(res, req, false, err.message, null, 400);
            } else {
                db.all(`SELECT * FROM users where id = ${user.id} LIMIT 1`, (err, row) => {
                    if (err || row == "") {
                        sendJsonResponse(res, req, false, err, null, 400);
                    } else {
                        var result = {};
                        result["user"] = row[0];
                        sendJsonResponse(res, req, true, "Successfully updated user!", result, 200);
                    }
                });
            }
        });
    }, req, res);
});

app.post('/api/login', async (req, res) => {
    db.all(`SELECT * FROM users where ((email = "${req.body.email}") AND (password = "${getHashedPassword(req.body.password)}"))`, (err, row) => {
        if (err || row == "") {
            var jsonResponse = {};
            jsonResponse["success"] = false;
            jsonResponse["message"] = "Email or password is incorrect.";
            jsonResponse["data"] = null;
            res.status(400).send(jsonResponse);
        } else {
            var result = {};
            result["user"] = row[0];
            var jsonResponse = {};
            jsonResponse["success"] = true;
            jsonResponse["message"] = "Successfully logged in!";
            jsonResponse["data"] = result;
            var token = generateAuthToken();
            authTokens[token] = result["user"];
            res.setHeader("auth-token", token);
            res.status(200).send(jsonResponse);
        }
    });
});

app.get('/api/check-login', async (req, res) => {
    if (!authTokens[req.header("auth-token")]) {
        await sendJsonResponse(res, req, false, "User not logged in!", null, 403)
    } else {
        var result = {};
        result["user"] = authTokens[req.header("auth-token")];
        sendJsonResponse(res, req, true, "User is logged in!", result, 200);
    }
});

app.get('/api/get-user', async (req, res) => {
    checkLogin(req.header("auth-token"), function (user) {
        db.all(`SELECT id, email, full_name, score, attended_no, not_attended FROM users where (id=${req.query.id})`, (err, row) => {
            if (err || row == "") {
                sendJsonResponse(res, req, false, "Email or password is incorrect.", null, 400);
            } else {
                var result = {};
                result["user"] = row[0];
                sendJsonResponse(res, req, true, "Successfully logged in!", result, 200);
            }
        });
    }, req, res);
});

app.get('/api/get-sports', async (req, res) => {
    checkLogin(req.header("auth-token"), function (user) {
        db.all(`SELECT * FROM sports`, (err, row) => {
            if (err) {
                sendJsonResponse(res, req, false, "Could not retreive sports.", null, 400);
            } else {
                sendJsonResponse(res, req, true, "Successfully retreived sports!", row, 200);
            }
        });
    }, req, res);
});

app.get('/api/get-sport', async (req, res) => {
    checkLogin(req.header("auth-token"), function (user) {
        db.all(`SELECT * FROM sports where id = "${req.query.id}"`, (err, row) => {
            if (err || row == "") {
                sendJsonResponse(res, req, false, "Could not retreive sport.", null, 400);
            } else {
                var result = {};
                result["sport"] = row[0];
                sendJsonResponse(res, req, true, "Successfully retreived sports!", result, 200);
            }
        });
    }, req, res);
});

app.get('/api/get-cities', async (req, res) => {
    checkLogin(req.header("auth-token"), function (user) {
        db.all(`SELECT * FROM cities`, (err, row) => {
            if (err) {
                sendJsonResponse(res, req, false, "Could not retreive sports.", null, 400);
            } else {
                sendJsonResponse(res, req, true, "Successfully retreived sports!", row, 200);
            }
        });
    }, req, res);
});

app.get('/api/get-city', async (req, res) => {
    checkLogin(req.header("auth-token"), function (user) {
        db.all(`SELECT * FROM cities where id = "${req.query.id}"`, (err, row) => {
            if (err || row == "") {
                sendJsonResponse(res, req, false, "Could not retreive city.", null, 400);
            } else {
                var result = {};
                result["city"] = row[0];
                sendJsonResponse(res, req, true, "Successfully retreived sports!", result, 200);
            }
        });
    }, req, res);
});

app.get('/api/get-event', async (req, res) => {
    // checkLogin(req.header("auth-token"), function (user) {
    db.all(`SELECT * FROM events where id = "${req.query.event_id}"`, (err, row) => {
        if (err || row == "") {
            sendJsonResponse(res, req, false, "Could not retreive event.", null, 400);
        } else {
            var result = {};
            result["event"] = row[0];
            sendJsonResponse(res, req, true, "Successfully retreived sports!", result, 200);
        }
    });
    // }, req, res);
});

app.get('/api/get-events', async (req, res) => {
    checkLogin(req.header("auth-token"), function (user) {
        db.all(`SELECT * FROM events where ((sport_id = ${req.query.sport_id}) AND (city_id = ${req.query.city_id}))`, (err, row) => {
            if (err) {
                sendJsonResponse(res, req, false, "Could not retreive sports.", null, 400);
            } else {
                sendJsonResponse(res, req, true, "Successfully retreived events!", row, 200);
            }
        });
    }, req, res);
});

app.post('/api/create-event', async (req, res) => {
    checkLogin(req.header("auth-token"), function (user) {
        db.run(`INSERT INTO events(name, sport_id, city_id, author_id, min_score, max_players, gym_name, gym_address, date, time, price, timestamp, type, gender, lat, lng) VALUES("${req.body.name}", ${req.body.sport_id},${req.body.city_id}, ${req.body.author_id},${req.body.min_score}, ${req.body.max_players}, "${req.body.gym_name}", "${req.body.gym_address}", "${req.body.date}", "${req.body.time}", "${req.body.price}", ${req.body.timestamp}, "${req.body.type}", "${req.body.gender}", "${req.body.lat}", "${req.body.lng}")`, function (err) {
            if (err) {
                sendJsonResponse(res, req, false, err.message, null, 400);
            } else {
                db.all(`SELECT * FROM events where id = ${this.lastID} LIMIT 1`, (err, row) => {
                    if (err || row == "") {
                        sendJsonResponse(res, req, false, err.message, null, 400);
                    } else {
                        var result = {};
                        result["event"] = row[0];
                        sendJsonResponse(res, req, true, "Successfully created event!", result, 200);
                    }
                });
            }
        });
    }, req, res);
});

app.post('/api/change-password', async (req, res) => {
    checkLogin(req.header("auth-token"), function (user) {
        db.run(`UPDATE users SET password="${getHashedPassword(req.body.new_password)}" where ((id=${req.body.id}) AND (password="${getHashedPassword(req.body.old_password)}"))`, function (err) {
            if (err) {
                sendJsonResponse(res, req, false, err.message, null, 400);
            } else {
                db.all(`SELECT * FROM users where id = ${req.body.id} LIMIT 1`, (err, row) => {
                    if (err || row == "") {
                        sendJsonResponse(res, req, false, err, null, 400);
                    } else {
                        var result = {};
                        result["user"] = row[0];
                        sendJsonResponse(res, req, true, "Successfully updated user!", result, 200);
                    }
                });
            }
        });
    }, req, res);
});

app.post('/api/add-user-onesignal-id', async (req, res) => {
    checkLogin(req.header("auth-token"), function (user) {
        db.run(`UPDATE users SET onesignal_id="${req.body.onesignal_id}" where (id=${user.id})`, function (err) {
            if (err) {
                sendJsonResponse(res, req, false, err.message, null, 400);
            } else {
                db.all(`SELECT * FROM users where id = ${user.id} LIMIT 1`, (err, row) => {
                    if (err || row == "") {
                        sendJsonResponse(res, req, false, err, null, 400);
                    } else {
                        var result = {};
                        result["user"] = row[0];
                        sendJsonResponse(res, req, true, "Successfully updated user!", result, 200);
                    }
                });
            }
        });
    }, req, res);
});

app.get('/api/get-event-participants', async (req, res) => {
    checkLogin(req.header("auth-token"), function (user) {
        db.all(`SELECT users.id, users.email, users.full_name, users.score, users.attended_no, users.not_attended, users.profile_picture, users.sport_id, users.city_id, joins.status as status FROM users CROSS JOIN joins ON (users.id=joins.user_id AND joins.event_id=${req.query.event_id} AND joins.status!="canceled")`, (err, row) => {
            if (err) {
                sendJsonResponse(res, req, false, "Could not retreive participants.", null, 400);
            } else {
                sendJsonResponse(res, req, true, "Successfully retreived participants!", row, 200);
            }
        });
    }, req, res);
});

app.get('/api/get-event-requests', async (req, res) => {
    checkLogin(req.header("auth-token"), function (user) {
        db.all(`SELECT users.id, users.email, users.full_name, users.score, users.attended_no, users.not_attended, users.profile_picture, users.sport_id, users.city_id, joinrequests.status as status FROM users CROSS JOIN joinrequests ON (users.id=joinrequests.user_id AND joinrequests.event_id=${req.query.event_id} AND joinrequests.status!="canceled" AND joinrequests.status!="approved")`, (err, row) => {
            if (err) {
                sendJsonResponse(res, req, false, "Could not retreive requests.", null, 400);
            } else {
                sendJsonResponse(res, req, true, "Successfully retreived requests!", row, 200);
            }
        });
    }, req, res);
});

app.post('/api/join-event', async (req, res) => {
    checkLogin(req.header("auth-token"), function (user) {
        db.all(`SELECT * FROM events WHERE id = ${req.body.event_id}`, function (err, row) {
            if (err) {
                sendJsonResponse(res, req, false, err.message, null, 400);
            } else {
                if (!row.length) {
                    sendJsonResponse(res, req, false, "Event not found.", null, 404);
                } else {
                    if (row[0].type == "public") {
                        db.all(`SELECT * FROM joins WHERE (event_id=${req.body.event_id} AND user_id=${user.id})`, function (err, row) {
                            if (err) {
                                sendJsonResponse(res, req, false, err.message, null, 400);
                            } else {
                                if (row.length) {
                                    db.run(`UPDATE joins SET status =  "attended" WHERE (event_id=${req.body.event_id} AND user_id=${user.id})`, function (err) {
                                        if (err) {
                                            sendJsonResponse(res, req, false, err.message, null, 400);
                                        } else {
                                            sendJsonResponse(res, req, true, "Successfully joined event", null, 200);
                                        }
                                    });
                                } else {
                                    db.run(`INSERT INTO joins(event_id, user_id) VALUES(${req.body.event_id}, ${user.id})`, function (err) {
                                        if (err) {
                                            sendJsonResponse(res, req, false, err.message, null, 400);
                                        } else {
                                            sendJsonResponse(res, req, true, "Successfully joined event", null, 200);
                                        }
                                    });
                                }
                            }
                        })
                    } else if (row[0].type == "approve_only") {
                        db.all(`SELECT * FROM joinrequests WHERE (event_id=${req.body.event_id} AND user_id=${user.id})`, function (err, row) {
                            if (err) {
                                sendJsonResponse(res, req, false, err.message, null, 400);
                            } else {
                                if (row.length) {
                                    db.run(`UPDATE joinrequests SET status =  "requested" WHERE (event_id=${req.body.event_id} AND user_id=${user.id})`, function (err) {
                                        if (err) {
                                            sendJsonResponse(res, req, false, err.message, null, 400);
                                        } else {
                                            sendJsonResponse(res, req, true, "Successfully requested to join event", null, 200);
                                        }
                                    });
                                } else {
                                    db.run(`INSERT INTO joinrequests(event_id, user_id) VALUES(${req.body.event_id}, ${user.id})`, function (err) {
                                        if (err) {
                                            sendJsonResponse(res, req, false, err.message, null, 400);
                                        } else {
                                            sendJsonResponse(res, req, true, "Successfully requested to join event", null, 200);
                                        }
                                    });
                                }
                            }
                        })
                    }
                }
            }
        })
    }, req, res);
});

app.post('/api/cancel-join', async (req, res) => {
    checkLogin(req.header("auth-token"), function (user) {
        db.run(`UPDATE joins SET status = "canceled" WHERE ((event_id=${req.body.event_id}) AND (user_id=${user.id}))`, function (err) {
            if (err) {
                sendJsonResponse(res, req, false, err.message, null, 400);
            } else {
                sendJsonResponse(res, req, true, "Successfully unjoined event", null, 200);
            }
        });
    }, req, res);
});

app.post('/api/cancel-request', async (req, res) => {
    checkLogin(req.header("auth-token"), function (user) {
        db.run(`UPDATE joinrequests SET status = "canceled" WHERE ((event_id=${req.body.event_id}) AND (user_id=${user.id}))`, function (err) {
            if (err) {
                sendJsonResponse(res, req, false, err.message, null, 400);
            } else {
                sendJsonResponse(res, req, true, "Successfully canceled request", null, 200);
            }
        });
    }, req, res);
});

app.post('/api/reject-request', async (req, res) => {
    checkLogin(req.header("auth-token"), function (user) {
        db.all(`SELECT * FROM events WHERE id = ${req.body.event_id}`, function (err, row) {
            if (err || row == "") {
                sendJsonResponse(res, req, false, "Event not found", null, 400);
            } else {
                if (row[0].author_id != user.id) {
                    sendJsonResponse(res, req, false, "You are not the admin of the event.", null, 400);
                } else {
                    db.run(`UPDATE joinrequests SET status = "rejected" WHERE ((event_id=${req.body.event_id}) AND (user_id=${req.body.user_id}))`, function (err) {
                        if (err) {
                            sendJsonResponse(res, req, false, err.message, null, 400);
                        } else {
                            sendJsonResponse(res, req, true, "Successfully ejected request", null, 200);
                        }
                    });
                }
            }
        });
    }, req, res);
});

app.post('/api/update-event', async (req, res) => {
    checkLogin(req.header("auth-token"), function (user) {
        db.all(`SELECT * FROM events WHERE id = ${req.body.event_id}`, function (err, row) {
            if (err || row == "") {
                sendJsonResponse(res, req, false, "Event not found", null, 400);
            } else {
                if (row[0].author_id != user.id) {
                    sendJsonResponse(res, req, false, "You are not the admin of the event.", null, 400);
                } else {
                    db.run(`UPDATE events SET min_score=${req.body.min_score}, max_players=${req.body.max_players}, price=${req.body.price}, date="${req.body.date}", time="${req.body.time}", timestamp=${req.body.timestamp} WHERE (id=${req.body.event_id})`, function (err) {
                        if (err) { 
                            sendJsonResponse(res, req, false, err.message, null, 400);
                        } else {
                            sendNotificationToEventParticipants(req, res, req.body.event_id);
                            sendJsonResponse(res, req, true, "Successfully ejected request", null, 200);
                        }
                    });
                }
            }
        });
    }, req, res);
});

app.post('/api/accept-join-request', async (req, res) => {
    checkLogin(req.header("auth-token"), function (user) {
        db.all(`SELECT * FROM events WHERE id = ${req.body.event_id}`, function (err, row) {
            if (err || row == "") {
                sendJsonResponse(res, req, false, "Event not found", null, 400);
            } else {
                if (row[0].author_id != user.id) {
                    sendJsonResponse(res, req, false, "You are not the admin of the event.", null, 400);
                } else {
                    db.run(`UPDATE joinrequests SET status = "approved" WHERE ((event_id=${req.body.event_id}) AND (user_id=${req.body.user_id}))`, function (err) {
                        if (err) {
                            sendJsonResponse(res, req, false, "Could not approve request.", null, 400);
                        } else {
                            db.all(`SELECT * FROM joins WHERE (event_id=${req.body.event_id} AND user_id=${req.body.user_id})`, function (err, row) {
                                if (err) {
                                    sendJsonResponse(res, req, false, err.message, null, 400);
                                } else {
                                    if (row.length) {
                                        db.run(`UPDATE joins SET status =  "attended" WHERE (event_id=${req.body.event_id} AND user_id=${req.body.user_id})`, function (err) {
                                            if (err) {
                                                sendJsonResponse(res, req, false, err.message, null, 400);
                                            } else {
                                                sendJsonResponse(res, req, true, "Successfully joined event", null, 200);
                                            }
                                        });
                                    } else {
                                        db.run(`INSERT INTO joins(event_id, user_id) VALUES(${req.body.event_id}, ${req.body.user_id})`, function (err) {
                                            if (err) {
                                                sendJsonResponse(res, req, false, err.message, null, 400);
                                            } else {
                                                sendJsonResponse(res, req, true, "Successfully joined event", null, 200);
                                            }
                                        });
                                    }
                                }
                            })
                        }
                    });
                }
            }
        });
    }, req, res);
})

app.post('/api/reset-auth-tokens', async (req, res) => {
    checkLogin(req.header("auth-token"), async function (user) {
        authTokens = {};
        await sendJsonResponse(res, req, true, "Successfully reset auth tokens", null, 200);
    }, req, res);
})

app.get('/api/logout', async (req, res) => {
    checkLogin(req.header("auth-token"), async function (user) {
        authTokens[req.header("auth-token")] = null;
        await sendJsonResponse(res, req, true, "Successfully logged out!", null, 200);
    }, req, res);
})

app.get('/api/get-joined-events', async (req, res) => {
    checkLogin(req.header("auth-token"), async function (user) {
        db.all(`SELECT * FROM events where id in (select event_id from joins where (user_id = ${user.id} AND status="attended"))`, (err, row) => {
            if (err) {
                sendJsonResponse(res, req, false, err, null, 400);
            } else {
                if (row == "") {
                    sendJsonResponse(res, req, false, "This user did not join any events yet!", null, 400);
                } else {
                    sendJsonResponse(res, req, true, "Successfully retreived events!", row, 200);
                }
            }
        });
    }, req, res);
})


app.post(
    "/api/update-profile-picture",
    upload.single("photo"),
    (req, res) => {
        checkLogin(req.header('auth-token'), async function (user) {
            const photoName = generateProfilePictureName();
            const tempPath = req.file.path;
            const targetPath = path.join(__dirname, "./uploads/" + photoName + ".png");

            if (path.extname(req.file.originalname).toLowerCase() === ".png" || path.extname(req.file.originalname).toLowerCase() === ".jpg") {
                fs.rename(tempPath, targetPath, err => {
                    if (err) {
                        sendJsonResponse(res, req, false, "Something went wrong (1)", null, 500);
                    } else {
                        db.run(`UPDATE users SET "profile_picture"="${photoName}.png" where id=${user.id}`, function (err) {
                            if (err) {
                                sendJsonResponse(res, req, false, err.message, null, 400);
                            } else {
                                db.all(`SELECT * FROM users where id = ${user.id} LIMIT 1`, (err, row) => {
                                    if (err || row == "") {
                                        sendJsonResponse(res, req, false, err, null, 400);
                                    } else {
                                        var result = {};
                                        result["user"] = row[0];
                                        sendJsonResponse(res, req, true, "Successfully updated user!", result, 200);
                                    }
                                });
                            }
                        });
                    }
                });
            } else {
                fs.unlink(tempPath, err => {
                    if (err) {
                        sendJsonResponse(res, req, false, "Something went wrong (2)", null, 500);
                    } else {
                        sendJsonResponse(res, req, false, "Only .png files are allowed!", null, 403);
                    }
                });
            }
        }, req, res);
    }
);

app.get('/images/*', function (req, res, path) {
    var imagePath = req.url,
        url = 'http://localhost:8081/uploads/' + imagePath.split('/')[2];

    res.sendFile(url);
});


app.post('/api/send-notif', async (req, res) => {
    checkLogin(req.header('auth-token'), async function (user) {
        players=[];
        players.push(user.onesignal_id);
        console.log(players);
        sendNotification(req, res, players, "message", { 'event_id': 14 });
        sendJsonResponse(res, req, true, "success", null, 200)
    }, req, res);
})



function sendNotificationToEventParticipants(req, res, event_id){
    db.all(`SELECT onesignal_id from users where (id in (SELECT user_id from joins where event_id=${event_id}))`, function (err, row){
        if(err){
            console.log(err);
        }else{
            var players = [];
            row.forEach(function(obj) { players.push(obj.onesignal_id); });
            sendNotification(req,res,players,"An event you are subscribed to was updated.", {'event_id': event_id});
        }
    });
}


function sendNotification(req, res, players, message, data) {
    const notification = {
        contents: {
            'en': message,
        },
        data: data,
        include_player_ids: players
    };
    client.createNotification(notification)
        .then(response => { })
        .catch(e => { console.log(e)});
}

function sendJsonResponse(res, req, success, message, data, code) {
    var jsonResponse = {};
    jsonResponse["success"] = success;
    jsonResponse["message"] = message;
    jsonResponse["data"] = data;
    res.setHeader("auth-token", req.header("auth-token") == undefined ? "" : req.header("auth-token"));
    res.status(code).send(jsonResponse);
}

const getHashedPassword = (password) => {
    const sha256 = crypto.createHash('sha256');
    const hash = sha256.update(password).digest('base64');
    return hash;
}

const generateAuthToken = () => {
    return crypto.randomBytes(30).toString('hex');
}

const generateProfilePictureName = () => {
    return crypto.randomBytes(30).toString('hex');
}

const checkLogin = (authToken, callback, req, res) => {
    if (authTokens[authToken]) {
        callback(authTokens[authToken]);
    } else {
        sendJsonResponse(res, req, false, "Login expired.", null, 401);
    }
}